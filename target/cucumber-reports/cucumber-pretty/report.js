$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("ObterListaUsuarios.feature");
formatter.feature({
  "comments": [
    {
      "line": 1,
      "value": "#Author: your.email@your.domain.com"
    },
    {
      "line": 2,
      "value": "#Keywords Summary :"
    },
    {
      "line": 3,
      "value": "#Feature: List of scenarios."
    },
    {
      "line": 4,
      "value": "#Scenario: Business rule through list of steps with arguments."
    },
    {
      "line": 5,
      "value": "#Given: Some precondition step"
    },
    {
      "line": 6,
      "value": "#When: Some key actions"
    },
    {
      "line": 7,
      "value": "#Then: To observe outcomes or validation"
    },
    {
      "line": 8,
      "value": "#And,But: To enumerate more Given,When,Then steps"
    },
    {
      "line": 9,
      "value": "#Scenario Outline: List of steps for data-driven as an Examples and \u003cplaceholder\u003e"
    },
    {
      "line": 10,
      "value": "#Examples: Container for s table"
    },
    {
      "line": 11,
      "value": "#Background: List of steps run before each of the scenarios"
    },
    {
      "line": 12,
      "value": "#\"\"\" (Doc Strings)"
    },
    {
      "line": 13,
      "value": "#| (Data Tables)"
    },
    {
      "line": 14,
      "value": "#@ (Tags/Labels):To group Scenarios"
    },
    {
      "line": 15,
      "value": "#\u003c\u003e (placeholder)"
    },
    {
      "line": 16,
      "value": "#\"\""
    },
    {
      "line": 17,
      "value": "## (Comments)"
    },
    {
      "line": 18,
      "value": "#Sample Feature Definition Template"
    }
  ],
  "line": 20,
  "name": "Definir cabeçalho de consulta e obter lista de usuários",
  "description": "",
  "id": "definir-cabeçalho-de-consulta-e-obter-lista-de-usuários",
  "keyword": "Feature",
  "tags": [
    {
      "line": 19,
      "name": "@obterlistausuarios"
    }
  ]
});
formatter.scenario({
  "line": 23,
  "name": "Recuperando lista de usuários",
  "description": "",
  "id": "definir-cabeçalho-de-consulta-e-obter-lista-de-usuários;recuperando-lista-de-usuários",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 22,
      "name": "@obterlistausuarios"
    }
  ]
});
formatter.step({
  "line": 24,
  "name": "Defino cabeçalhos e parâmetros para solicitação",
  "rows": [
    {
      "cells": [
        "KEY",
        "VALUE"
      ],
      "line": 25
    },
    {
      "cells": [
        "page",
        "2"
      ],
      "line": 26
    }
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 27,
  "name": "O usuário acessou o serviço da web https://api.trello.com/1/actions/592f11060f95a3d3d46a987a",
  "keyword": "Then "
});
formatter.step({
  "line": 28,
  "name": "Imprimo todos os logs no console",
  "keyword": "And "
});
formatter.step({
  "line": 29,
  "name": "O código de status é 200",
  "keyword": "Then "
});
formatter.match({
  "location": "ObterListaUsuariosSteps.defino_cabeçalhos_e_parâmetros_para_solicitação(DataTable)"
});
formatter.result({
  "duration": 916492701,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "https://api.trello.com/1/actions/592f11060f95a3d3d46a987a",
      "offset": 35
    }
  ],
  "location": "ObterListaUsuariosSteps.o_usuário_acessou_o_serviço_da_web_https_reqres_in_api_users(String)"
});
formatter.result({
  "duration": 1057586100,
  "status": "passed"
});
formatter.match({
  "location": "ObterListaUsuariosSteps.imprimo_todos_os_logs_no_console()"
});
formatter.result({
  "duration": 17694300,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 21
    }
  ],
  "location": "ObterListaUsuariosSteps.o_código_de_status_é(int)"
});
formatter.result({
  "duration": 25106200,
  "status": "passed"
});
formatter.after({
  "duration": 75001,
  "status": "passed"
});
});