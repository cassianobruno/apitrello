package br.com.frameworktestng.api.steps;

import static io.restassured.RestAssured.given;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import br.com.frameworktestng.api.execution.ObterListaUsuariosExecution;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

public class ObterListaUsuariosSteps {

	protected ObterListaUsuariosExecution obterExe;
	
	@After("@obterlistausuarios")
	public void leave_window_open(Scenario scenario) {
		if (scenario.isFailed()) {
			System.out.println("Falhou");
		} else {
			System.out.println("Passou");
		}
	}

	@Given("^Defino cabeçalhos e parâmetros para solicitação$")
	public void defino_cabeçalhos_e_parâmetros_para_solicitação(DataTable dt) throws Throwable {
		obterExe = new ObterListaUsuariosExecution();
		obterExe.definirCabecalho(dt);
	}

	@Then("^O usuário acessou o serviço da web (.*)")
	public void o_usuário_acessou_o_serviço_da_web_https_reqres_in_api_users(String WebServiceURL) throws Throwable {
		obterExe = new ObterListaUsuariosExecution();
		obterExe.setUrlResponse(WebServiceURL);
	}

	@Then("^Imprimo todos os logs no console$")
	public void imprimo_todos_os_logs_no_console() throws Throwable {
		obterExe = new ObterListaUsuariosExecution();
		obterExe.imprimirLogCampo("id");
	}

	@Then("^O código de status é (\\d+)$")
	public void o_código_de_status_é(int statusCode) throws Throwable {
		obterExe = new ObterListaUsuariosExecution();
		obterExe.validaStatusCode(statusCode);
	}

}
