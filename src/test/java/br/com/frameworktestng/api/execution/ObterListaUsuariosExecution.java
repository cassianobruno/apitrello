package br.com.frameworktestng.api.execution;

import static io.restassured.RestAssured.given;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import cucumber.api.DataTable;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

public class ObterListaUsuariosExecution<Account> {

	private static String url;
	private static Response response;
	private static ValidatableResponse json;
	private static RequestSpecification request;
	
	public static String getUrl() {
		return url;
	}

	public static void setUrl(String url) {
		ObterListaUsuariosExecution.url = url;
	}

	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}

	public ValidatableResponse getJson() {
		return json;
	}

	public void setJson(ValidatableResponse json) {
		this.json = json;
	}

	public RequestSpecification getRequest() {
		return request;
	}

	public void setRequest(RequestSpecification request) {
		this.request = request;
	}
	
	public void definirCabecalho(DataTable dt) {
		List<Map<String, String>> list = dt.asMaps(String.class, String.class);
		for(int i=0; i<list.size(); i++) {
			if(StringUtils.isNumeric(list.get(i).get("VALUE"))){
				setRequest(given().param(list.get(i).get("KEY"), Integer.parseInt(list.get(i).get("VALUE"))));
			}
			else{
				given().param(list.get(i).get("KEY"), list.get(i).get("VALUE")); 
			}
		}
	}
	
	public void setUrlResponse(String WebServiceURL) {
		setResponse(getRequest().when().get(WebServiceURL));
		setUrl(WebServiceURL);
		//System.out.println("response: " + getResponse().prettyPrint());
	}

	public void incluirParametro(String parametro, String valor) {
		setUrl(getUrl() + "/get?" + parametro + "=" + valor);
	}
	
	public void incluirSegundoParametro(String parametro, String valor) {
		setUrl(getUrl() + "&" + parametro + "=" + valor);
	}

	public void imprimirLog() {
		getResponse().then().log().all();
	}

	public void validaStatusCode(int statusCode) {
		setJson(getResponse().then().statusCode(statusCode));
	}
	
	public void validaObjeto(String retorno,  String valorRetorno) {
		String valorEsperado = getResponse().jsonPath().getString(retorno);
		Assert.assertEquals(valorEsperado, valorRetorno);
	}
	
	public void imprimirLogCampo(String campo) {
		System.out.println(getResponse().getBody().asString());
		
		JSONObject jsonObject =new JSONObject(getResponse().getBody().asString());
		JSONObject jsonObjectArray = jsonObject.getJSONObject("data");
		JSONObject JsonObjectList = jsonObjectArray.getJSONObject("list");
		
		for(int i=0;i<jsonObjectArray.getJSONObject("list").length()-1;i++){
		    JSONObject json = jsonObjectArray.getJSONObject("list");
		    String id = json.getString("id");
		    String name=json.getString("name");
		    
		    System.out.println("ID: " + id);
		    System.out.println("NAME: " + name);
		    
		}
		
	}
}
