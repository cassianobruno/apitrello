package br.com.frameworktestng.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class lerProperties {

	public static Properties getProperties() throws IOException {
		String fileProperties = ".\\src\\test\\resources\\parametros\\dados.properties";
		Properties properties = new Properties();
		FileInputStream file = new FileInputStream(fileProperties);
		properties.load(file);
		return properties;
	}
	
	public static void main(String[] args) throws IOException {
		Properties prop = getProperties();
		System.out.println(prop.getProperty("diaExec"));
	}
}
