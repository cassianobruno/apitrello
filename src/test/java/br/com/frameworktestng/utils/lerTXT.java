package br.com.frameworktestng.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Writer;
import java.util.HashMap;

public class lerTXT {
	
	public static String ultimaAtualizacao() {
		String ultimo = "";
		try {
			InputStream is = new FileInputStream(".\\src\\test\\resources\\parametros\\ultimaexecucao.txt");
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);

			String line = "";
			while (line != null) {
				line = br.readLine();
				if (line != null) {
					ultimo = line;
				}
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ultimo;
	}
	
	public static int diaSemanaExec() {
		String diaSemana = "";
		int dia = 0;
		try {
			InputStream is = new FileInputStream(".\\src\\test\\resources\\parametros\\properties.txt");
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);

			String line = "";
			int lineNumber = 0;
			HashMap<Integer, String> lines = new HashMap<Integer, String>();
			
			while ((line = br.readLine()) != null) {
				lines.put(lineNumber, line);
				lineNumber++;
			}
			
			br.close();
			
			diaSemana = lines.get(0);
			diaSemana = diaSemana.substring(8, 9);
			dia = Integer.parseInt(diaSemana);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return dia;
	}
	
	public static String nameProject() {
		String nameProject = "";
		String name = "";
		try {
			InputStream is = new FileInputStream(".\\src\\test\\resources\\parametros\\properties.txt");
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);

			String line = "";
			int lineNumber = 0;
			HashMap<Integer, String> lines = new HashMap<Integer, String>();
			
			while ((line = br.readLine()) != null) {
				lines.put(lineNumber, line);
				lineNumber++;
			}
			
			br.close();
			
			nameProject = lines.get(2);
			nameProject = nameProject.substring(12, 30);
			name = nameProject;
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return name;
	}
	
	public static String horarioExec() {
		String diaSemana = "";
		try {
			InputStream is = new FileInputStream(".\\src\\test\\resources\\parametros\\properties.txt");
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);

			String line = "";
			int lineNumber = 0;
			HashMap<Integer, String> lines = new HashMap<Integer, String>();
			
			while ((line = br.readLine()) != null) {
				lines.put(lineNumber, line);
				lineNumber++;
			}
			
			br.close();
			
			diaSemana = lines.get(1);
			diaSemana = diaSemana.substring(12, 20);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return diaSemana;
	}
	
	public static void gravarExec(String dataExec) {
		try {
			String is = ".\\src\\test\\resources\\parametros\\ultimaexecucao.txt";
			FileWriter isr = new FileWriter(is, true);
			BufferedWriter br = new BufferedWriter(isr);

			br.write(dataExec + "\n");
			
			br.close();			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
