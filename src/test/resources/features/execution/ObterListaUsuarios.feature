#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@obterlistausuarios 
Feature: Definir cabeçalho de consulta e obter lista de usuários 

@obterlistausuarios 
Scenario: Recuperando lista de usuários 
	Given Defino cabeçalhos e parâmetros para solicitação 
		|KEY |VALUE|
		|page|2    |
	Then O usuário acessou o serviço da web https://api.trello.com/1/actions/592f11060f95a3d3d46a987a 
	And Imprimo todos os logs no console 
	Then O código de status é 200